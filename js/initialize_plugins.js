//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins START  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/




		var sticky = $(".sticky"),
			tabs = $('.tabs'),
			matchheight = $(".matchheight"),
		    styler = $(".styler"),
		    owl = $(".owl-carousel"),
		    flex = $(".flexslider"),
		    royalslider = $(".royalslider"),
			wow = $(".wow"),
			popup = $("[data-popup]"),
			phone = $(".phone"),
			accordion = $('.accordion');


			if(sticky.length){
					include("plugins/sticky.js");
					include("plugins/jquery.smoothscroll.js");
			}
			if(phone.length){
					include('plugins/maskedInput.js');
			}
			if(accordion.length){
					include("plugins/jquery-ui.js");
			}
			if(matchheight.length){
					include("plugins/jquery.matchHeight-min.js");
			}
			if(styler.length){
					include("plugins/formstyler/formstyler.js");
			}
			if(tabs.length){
					include("plugins/easy-responsive-tabs/easyResponsiveTabs.js");
			}
			if(popup.length){
					include('plugins/arcticmodal/jquery.arcticmodal.js');
			}
			if(owl.length){
					include('plugins/owl-carousel/owl.carousel.js');
			}

					include("plugins/modernizr.js");



			function include(url){ 

					document.write('<script src="'+ url + '"></script>'); 

			}

		


		$(document).ready(function(){



			/* ------------------------------------------------
				Accordion
			------------------------------------------------ */

			if(accordion.length){

				accordion.accordion({
					heightStyle: "content"
				});
			}

			/* ------------------------------------------------
			MASKEDINPUT START
			------------------------------------------------ */

				if(phone.length){
			        // $("#date").mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
			        $(".phone").mask("7(999) 999-99-99");
				}


			/* ------------------------------------------------
			STICKY START
			------------------------------------------------ */

					if (sticky.length){
						$(sticky).sticky({
					        topspacing: 0,
					        styler: 'is-sticky',
					        animduration: 0,
					        unlockwidth: false,
					        screenlimit: false,
					        sticktype: 'alonemenu'
						});
					};

			/* ------------------------------------------------
			STICKY END
			------------------------------------------------ */




			/* ------------------------------------------------
			FORMSTYLER START
			------------------------------------------------ */

					if (styler.length){
						styler.styler({
							// selectSmartPositioning: true
						});
					}

			/* ------------------------------------------------
			FORMSTYLER END
			------------------------------------------------ */


			/* ------------------------------------------------
			OWL START
			------------------------------------------------ */

					if(owl.length){
						owl.owlCarousel({
							singleItem : true,
							items : 1,
							// loop: true,
							smartSpeed:1000,
							nav: true
							// autoHeight:true
						});
					}

					// if(owl.length){
					//     owl.each(function(){
					//     	var $this = $(this),
					//       		items = $this.data('items');

					//     	$this.owlCarousel({
					//     		singleItem : true,
					// 			items : 1,
					// 			// loop: true,
					// 			smartSpeed:1000,
					// 			// autoHeight:true,
					//     		dots:false,
					//     		nav: true,
					//             navText: [ '', '' ],
					//             // margin: 30,
					//             responsive : items
					//     	});
					//     });
					// }
					// <div class="owl-carousel" data-items='{  "0":{"items":1},   "480":{"items":2},   "991":{"items":3}  }'></div>

			/* ------------------------------------------------
			OWL END
			------------------------------------------------ */




			/* ------------------------------------------------
			TABS START
			------------------------------------------------ */

					if(tabs.length){
						tabs.easyResponsiveTabs();
					}

			/* ------------------------------------------------
			TABS END
			------------------------------------------------ */



			/* ------------------------------------------------
			POPUP START
			------------------------------------------------ */

					if(popup.length){
						popup.on('click',function(){
						    var modal = $(this).data("popup");
						    $(modal).arcticmodal();
						});
					};

			/* ------------------------------------------------
			POPUP END
			------------------------------------------------ */



		});




//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins END    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================
