$(function(){
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ




$(document).ready(function(){


		// $("a.btn").on("click", function (event) {
	 //        event.preventDefault();
	 //        var id  = $(this).attr('href'),
	 //            top = $(id).offset().top;
	 //        $('body,html').animate({scrollTop: top}, 1500);
	 //    });

		var buff=$(".price span").text();
		$('#iphone-price').val(buff);
		$(document).on('change','.trigger',function(){
			if($('#m1').prop('checked') && $('#c1').prop('checked')){
				$('.photo img').removeClass("active");
				$('.photo').children('img').eq(5).addClass("active");
				
				$('#iphone-model').val("IPhone 6s");
				$('#iphone-color').val("Серебристый");
			}
			else if($('#m1').prop('checked') && $('#c2').prop('checked')){
				$('.photo img').removeClass("active");
				$('.photo').children('img').eq(4).addClass("active");
				
				$('#iphone-model').val("IPhone 6s");
				$('#iphone-color').val("Золотой");
			}
			else if($('#m1').prop('checked') && $('#c3').prop('checked')){
				$('.photo img').removeClass("active");
				$('.photo').children('img').eq(7).addClass("active");
				
				$('#iphone-model').val("IPhone 6s");
				$('#iphone-color').val("Серый космос");
			}
			else if($('#m1').prop('checked') && $('#c4').prop('checked')){
				$('.photo img').removeClass("active");
				$('.photo').children('img').eq(6).addClass("active");
				
				$('#iphone-model').val("IPhone 6s");
				$('#iphone-color').val("Розовое золото");
			}
			else if($('#m2').prop('checked') && $('#c1').prop('checked')){
				$('.photo img').removeClass("active");
				$('.photo').children('img').eq(1).addClass("active");
				
				$('#iphone-model').val("IPhone 6s Plus");
				$('#iphone-color').val("Серебристый");
			}
			else if($('#m2').prop('checked') && $('#c2').prop('checked')){
				$('.photo img').removeClass("active");
				$('.photo').children('img').eq(0).addClass("active");
				
				$('#iphone-model').val("IPhone 6s Plus");
				$('#iphone-color').val("Золотой");
			}
			else if($('#m2').prop('checked') && $('#c3').prop('checked')){
				$('.photo img').removeClass("active");
				$('.photo').children('img').eq(3).addClass("active");
				
				$('#iphone-model').val("IPhone 6s Plus");
				$('#iphone-color').val("Серый космос");
			}
			else if($('#m2').prop('checked') && $('#c4').prop('checked')){
				$('.photo img').removeClass("active");
				$('.photo').children('img').eq(2).addClass("active");
				
				$('#iphone-model').val("IPhone 6s Plus");
				$('#iphone-color').val("Розовое золото");
			}

			else if($('#m3').prop('checked') && $('#c1').prop('checked')){
				$('.photo img').removeClass("active");
				$('.photo').children('img').eq(1).addClass("active");
				
				$('#iphone-model').val("IPhone 6s Plus");
				$('#iphone-color').val("Серебристый");
			}
			else if($('#m3').prop('checked') && $('#c2').prop('checked')){
				$('.photo img').removeClass("active");
				$('.photo').children('img').eq(0).addClass("active");
				
				$('#iphone-model').val("IPhone 6s Plus");
				$('#iphone-color').val("Золотой");
			}
			else if($('#m3').prop('checked') && $('#c3').prop('checked')){
				$('.photo img').removeClass("active");
				$('.photo').children('img').eq(3).addClass("active");
				
				$('#iphone-model').val("IPhone 6s Plus");
				$('#iphone-color').val("Серый космос");
			}
			else if($('#m3').prop('checked') && $('#c4').prop('checked')){
				$('.photo img').removeClass("active");
				$('.photo').children('img').eq(2).addClass("active");
				
				$('#iphone-model').val("IPhone 6s Plus");
				$('#iphone-color').val("Розовое золото");
			}
			
			if($('#memo1').prop('checked')){
				$('#iphone-volume').val("2 ядерный");
			}
			if($('#memo2').prop('checked')){
				$('#iphone-volume').val("4 ядерный");
			}
			if($('#memo3').prop('checked')){
				$('#iphone-volume').val("8 ядерный");
			}
			 
			 
			 
			if($('#memo1').prop('checked') && $('#m1').prop('checked') && $('#c4').prop('checked')){
				$('#iphone-price').val("6990 руб.");
				$(".price span").text("6990 руб.");
					
			}
			else if($('#memo2').prop('checked') && $('#m1').prop('checked') && $('#c4').prop('checked')){
				$('#iphone-price').val("9590 руб.");
				$(".price span").text("9590 руб.");
				
			}
			else if($('#memo3').prop('checked') && $('#m1').prop('checked') && $('#c4').prop('checked')){
				$('#iphone-price').val("Нет в наличии");
				$(".price span").text("Нет в наличии");
			}
			else if($('#memo1').prop('checked') && $('#m2').prop('checked') && $('#c4').prop('checked')){
				$('#iphone-price').val("7990 руб.");
				$(".price span").text("7990 руб.");
			}
			else if($('#memo2').prop('checked') && $('#m2').prop('checked') && $('#c4').prop('checked')){
				$('#iphone-price').val("9 990 руб.");
				$(".price span").text("9 990 руб.");
			}
			else if($('#memo3').prop('checked') && $('#m2').prop('checked') && $('#c4').prop('checked')){
				$('#iphone-price').val("Нет в наличии");
				$(".price span").text("Нет в наличии");
			}
			
			
			else if($('#memo1').prop('checked') && $('#m1').prop('checked')){
				$('#iphone-price').val("6990 руб.");
				$(".price span").text("6990 руб.");
			}
			else if($('#memo2').prop('checked') && $('#m1').prop('checked')){
				$('#iphone-price').val("9590 руб.");
				$(".price span").text("9590 руб.");
			}
			else if($('#memo3').prop('checked') && $('#m1').prop('checked')){
				$('#iphone-price').val("20590 руб.");
				$(".price span").text("20590 руб.");
			}
			else if($('#memo1').prop('checked') && $('#m2').prop('checked')){
				$('#iphone-price').val("7990 руб.");
				$(".price span").text("7990 руб.");
			}
			else if($('#memo2').prop('checked') && $('#m2').prop('checked')){
				$('#iphone-price').val("9 990 руб.");
				$(".price span").text("9 990 руб.");
			}
			else if($('#memo3').prop('checked') && $('#m2').prop('checked')){
				$('#iphone-price').val("Нет в наличии");
				$(".price span").text("Нет в наличии");
			}
			else if($('#memo1').prop('checked') && $('#m3').prop('checked')){
				$('#iphone-price').val("7990 руб.");
				$(".price span").text("7990 руб.");
			}
			else if($('#memo2').prop('checked') && $('#m3').prop('checked')){
				$('#iphone-price').val("9 990 руб.");
				$(".price span").text("9 990 руб.");
			}
			else if($('#memo3').prop('checked') && $('#m3').prop('checked')){
				$('#iphone-price').val("12 990 руб");
				$(".price span").text("12 990 руб");
			}
			else{$('#iphone-price').val(buff);$(".price span").text(buff);}
	/*
			var price = 0;
			var currentPrice = 0;
			
			if($('#m1').prop('checked')){
				$('.trigger').each(function(){
					if($(this).prop('checked')){
						currentPrice = $(this).attr('data-price-6');
						currentPrice = parseInt(currentPrice,10);
						price += currentPrice;
					}
				});
			}
			
			if($('#m2').prop('checked')){
				$('.trigger').each(function(){
					if($(this).prop('checked')){
						currentPrice = $(this).attr('data-price-6s');
						currentPrice = parseInt(currentPrice,10);
						price += currentPrice;
					}
				});
			}
			$(".price span").text(price);*/
		});
		
		$('.trigger').eq(1).trigger("click");
		$('.trigger').eq(0).trigger("click");






		/* ------------------------------------------------
		ANCHOR START
		------------------------------------------------ */
				 
			    function goUp(){
					var windowHeight = $(window).height(),
						windowScroll = $(window).scrollTop();

					if(windowScroll>windowHeight/2){
						$('.arrow_up').addClass('active');
					}

					else{
						$('.arrow_up').removeClass('active');
					}

			    }

			    goUp();
				$(window).on('scroll',goUp);

				$('.arrow_up').on('click ontouchstart',function () {

					if($.browser.safari){
						$('body').animate( { scrollTop: 0 }, 1100 );
					}
					else{
						$('html,body').animate( { scrollTop: 0}, 1100 );
					}
					return false;
					
				});

		/* ------------------------------------------------
		ANCHOR END
		------------------------------------------------ */



		/* ------------------------------------------------
		RESPONSIVE MENU START
		------------------------------------------------ */

				$(".resp_btn, .close_resp_menu").on("click ontouchstart", function(){
					$("body").toggleClass("show_menu")
				});

				// $(document).on("click ontouchstart", function(event) {
			 //      if ($(event.target).closest("nav,.resp_btn").length) return;
			 //      $("body").removeClass("show_menu");
			 //      if(windowW <= 991){
			 //      	$(".menu_item").removeClass("active").find(".dropdown-menu").css("display","none");
			 //      }
			 //      event.stopPropagation();
			 //    });

				// проверка на наличие элемента и вставка хтмл кода
			 	//  if($(window).width() <= 767){
				// 	if ($(".menu_item").length){
				//         $(".menu_item").has('.dropdown-menu').append('<span class="touch-button"><i class="fa fa-angle-down"></i></span>');
				//     }
				// }
				// проверка на наличие элемента и вставка хтмл кода


				$('.menu_link').on('click ontouchstart',function(event){
					if($("html").hasClass("md_no-touch"))return;

			        var windowWidth = $(window).width(),
			            $parent = $(this).parent('.menu_item');
			        if(windowWidth > 991){
			          // if($("html").hasClass("md_touch")){
			            if((!$parent.hasClass('active')) && $parent.find('.dropdown-menu').length){

			              event.preventDefault();

			              $parent.toggleClass('active')
			               .siblings()
			               .find('.menu_link')
			               .removeClass('active');
			            }
			          // }  
			        }
			        
			        else{
			            
			          if((!$parent.hasClass('active')) && $parent.find('.dropdown-menu').length){

			            event.preventDefault();

			            $parent.toggleClass('active')
			             .siblings()
			             .removeClass('active');
			            $parent.find(".dropdown-menu")
			             .slideToggle()
			             .parents('.menu_item')
			             .siblings()
			             .find(".dropdown-menu")
			             .slideUp();
			          }
			        }

			    });



					// $('.categories_lk , .sale_primary_nav__list > li > a, .sale_primary_nav__list_link').on('click ontouchstart',function(event){
			  //       	if($("html").hasClass("md_no-touch"))return;  

			  //           var windowWidth = $(window).width(),
			  //               $parent = $(this).parent('.menu_categories > li, .sale_primary_nav__list > li, .menu_categories_item, .sale_primary_nav__list_it');
			  //           if(windowWidth > 767){
			  //             // if($("html").hasClass("md_touch")){
			  //               if((!$parent.hasClass('active')) && $parent.find('[class*="subcategories_show"]').length){

			  //                 event.preventDefault();

			  //                 $parent.toggleClass('active')
			  //                  .siblings()
			  //                  .find('.categories_lk , .sale_primary_nav__list > li > a, .sale_primary_nav__list_link')
			  //                  .removeClass('active');
			  //               }
			  //             // }  
			  //           }
			            
			  //           else{
			                
			  //             if((!$parent.hasClass('active')) && $parent.find('[class*="subcategories_show"]').length){

			  //               event.preventDefault();

			  //               $parent.toggleClass('active')
			  //                .siblings()
			  //                .removeClass('active');
			  //               $parent.find("[class*='subcategories_show']")
			  //                .slideToggle()
			  //                .parents('.menu_categories > li, .sale_primary_nav__list > li, .menu_categories_item, .sale_primary_nav__list_it')
			  //                .siblings()
			  //                .find("[class*='subcategories_show']")
			  //                .slideUp();
			  //             }
			  //           }

			  //       });


			  //       $(document).on("click ontouchstart", function(event) {
			  //         if ($(event.target).closest(".categories_lk , .sale_primary_nav__list > li > a, .sale_primary_nav__list_link").length) return;
			  //         $(".menu_categories > li, .sale_primary_nav__list > li, .menu_categories_item, .sale_primary_nav__list_it").removeClass("active");
			  //         $(".menu_categories > li, .sale_primary_nav__list > li, .menu_categories_item, .sale_primary_nav__list_it").find("[class*='subcategories_show']").css("display","none");
			  //         event.stopPropagation();
			  //       });

		/* ------------------------------------------------
		RESPONSIVE MENU END
		------------------------------------------------ */



		
});